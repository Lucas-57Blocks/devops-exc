const http = require('http');
const express = require('express');
const path = require('path');


const app = express();
app.server = http.createServer(app);

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.get('/', (req, res) => {
  res.render('home');
});
app.get('/result/:result', (req, res) => {
  const { result } = req.params;
  res.render('result', { result });
});

app.get('/api/sum', (req, res) => {
  console.log('body', req.query);
  const { num1, num2 } = req.query;
  res.json({ result:  parseFloat(num1) + parseFloat(num2) });
  res.end();
});

app.server.listen(process.env.PORT || 8080, () => {
  console.log(`Listening on port ${app.server.address().port}`);
});

module.exports = app;
